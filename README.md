# Fritzy monitoring statping

Deploy statping monitoring software

## common environment variables used to deploy

- DEPLOY_HOSTNAME_IDENTITY=**mandatory** *ansible ssh private key*
- HCLOUD_TOKEN=**mandatory** *hetzner api token*
- LETS_ENCRYPT_DNS_PROVIDER=**mandatory** *dns provider for let's encrypt certificate*
- TRAEFIK_WILDCARD_DOMAIN=**example.com** *Traefik wilcard domain*

Statping will be deployed as swarm service *monitoring_statping*.

```plantuml
@startuml
cloud loadbalancer

card swarm_cluster {

  node statping.1

  folder gluster [
    /mnt/ha/statping
    ---
    sqlite statping database
  ]
}

loadbalancer -> statping.1 : STATPING_DOMAIN:443
statping.1 -> gluster
@enduml
```

### Statping access configuration

- ADMIN_PASSWORD  = **admin if not specified** *Password for administrator account*
- ADMIN_USER      = **admin if not specified** *Username for administrator account*
- API_SECRET      = **optional** *Set a custom API Secret for API Authentication*
- DESCRIPTION     = **optional** *Set a description for the Statping status page*
- NAME            = **optional** *Set a name for the Statping status page*
- STATPING_DOMAIN = **statping.{{TRAEFIK_WILDCARD_DOMAIN}} if not specified** *statping url*
